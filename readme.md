date: 21/08/2019
author: j.m. wilms
language: python version 3.6
package dependencies: scipy, numpy, pandas, os, xlswriter
description: This script searches the output folder of catman for .MAT (matlab) files and translates the information to an excel readable file.
