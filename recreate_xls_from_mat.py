"""
date: 21/08/2019
author: j.m. wilms
language: python version 3.6
package dependencies: scipy, numpy, pandas, os, xlswriter
description: This script searches the output folder of catman for .MAT (matlab) files and translates the information to an excel readable file.
"""
from scipy.io import loadmat
import numpy as np
import pandas as pd
import os
import xlsxwriter

dp = os.path.join(os.path.abspath(os.path.dirname('__file__')),'..','INPUT','file_voorbeelden_geocentrifuge')
dp_out = os.path.join(os.path.abspath(os.path.dirname('__file__')),'..','OUTPUT')
if not os.path.exists(dp_out): #if the output directory does not exist, create it.
	os.makedirs(dp_out)
fnames = os.listdir(dp) #obtain all filenames in directory
fnames = [f for f in fnames if f.endswith('.MAT')] #isolate matlab files

for f in fnames:
	DATA = []
	print(f)
	mat = loadmat(os.path.join(dp,f))
	column_names = []
	unit_names = []
	df0 = pd.DataFrame()
	df1 = pd.DataFrame()
	H = []
	D = []
	for k in mat.keys():
		if k not in (['__header__', '__version__', '__globals__','File_Header']):


			if k.endswith('Header'):
				H.append(k)
				unit_names.append(mat[k][-1][-1][1][0]) #get the units
				column_names.append(mat[k][-1][-1][-1][0]) #get the column names
			if k.endswith('Data'):
				D.append(k)
				vals = np.array(mat[k]).flatten()

				DATA.append(vals)

	C = list(zip(H,D))
	"""check to ensure that channel header matches channel values"""
	for v in C:
		v0 = v[0].split('Header')[0]
		v1 =v[1].split('Data')[0]
		if v0!=v1:
			print('Discrepancy, check column names')
			break
	column_names2 = [list(column_names),list(unit_names)]


	for i,cname in enumerate(column_names):
		df1[cname] = DATA[i]
	"""use multi-index so that column header contains both name and units in subsequent rows"""
	df1.columns = pd.MultiIndex.from_arrays(column_names2)
	"""write to excel file within the same folder"""
	fp = os.path.join(dp_out,'mat_%s.xls' % str(f[:-4])) #create path to file in OUTPUT with same name as mat file
	df1.to_excel(fp, engine='xlsxwriter')

